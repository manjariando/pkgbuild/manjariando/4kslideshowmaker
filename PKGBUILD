# Maintainer: Muflone http://www.muflone.com/contacts/english/

pkgname=4kslideshowmaker
pkgver=1.8.2.1041
pkgrel=2.3
pkgdesc="A straightforward and easy-to-use app to create slideshows from music and photos, with Instagram support"
arch=('x86_64')
url="https://www.4kdownload.com/products/product-slideshowmaker"
license=('custom:eula')
makedepends=('chrpath' 'imagemagick')
#install=${pkgname}.install
source=("${pkgname}_${pkgver}_amd64.tar.bz2"::"https://dl.4kdownload.com/app/${pkgname}_${pkgver%.*}_amd64.tar.bz2"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        'fix_symlink_path.patch')
sha256sums=('2c87658444df5451f04ca8d4daaa534c93e4684689721389ae8382065e40d361'
            '2fce041ee008d47611e9b3ae400e2ac00799bda3ce1b078e2353af3e10572916'
            '5f8c0df073993afe8de7600309dc188a54a4ec0d1055485e709618c14f0a0a88'
            '296e875b47b6a5e69688d12f145ada3bf4683a05f3a0153adee1577e6dbb179c')

prepare() {
    cd "${pkgname}"
    # Remove insecure RPATH
    chrpath --delete "${pkgname}-bin"

    # Fix symlink path
    patch -p1 -i "${srcdir}/fix_symlink_path.patch"
}

_4kslideshowmaker_desktop="[Desktop Entry]
Name=4K Slideshow Maker
GenericName=4K Slideshow Maker
GenericName[pt_BR]=Criador de apresentação de slides em 4K
Comment=Create impressive slideshows, save your memories
Comment[pt_BR]=Crie apresentações de slides impressionantes, salve suas memórias
Exec=4kslideshowmaker
Terminal=false
Type=Application
Icon=4kslideshowmaker.png
Categories=Graphics;Qt;"

build() {
    cd "${srcdir}"
    echo -e "$_4kslideshowmaker_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('openssl')

    # Install files
    install -m 755 -d "${pkgdir}/usr/lib"
    cp -r "${pkgname}" "${pkgdir}/usr/lib"
    chown root.root "${pkgdir}/usr/lib/${pkgname}"

    # Install launcher file
    install -m 755 -d "${pkgdir}/usr/bin"
    ln -s "/usr/lib/${pkgname}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
    # Install license file
    install -m 755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
    install -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" "${pkgname}/doc/eula"
        
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
